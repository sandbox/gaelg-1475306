(function($) {

  /**
   * Overrides js behavior from user.permissions.js to extend it.
   * Shows checked and disabled checkboxes for inherited permissions.
   * The system is made in such a way that each checkbox is depending from at
   * most one parent checkbox, according to the roles tree. This eases dynamic
   * checking.
   */
  Drupal.behaviors.permissions = {
    attach : function(context) {

      var self = this;
      $('table#permissions').once('cascading_roles', function() {
        // On a site with many roles and permissions, this behavior initially has
        // to perform thousands of DOM manipulations to inject checkboxes and hide
        // them. By detaching the table from the DOM, all operations can be
        // performed without triggering internal layout and re-rendering processes
        // in the browser.
        var $table = $(this);
        if($table.prev().length) {
          var $ancestor = $table.prev(), method = 'after';
        }
        else {
          var $ancestor = $table.parent(), method = 'append';
        }
        $table.detach();

        // Create dummy checkboxes. We use dummy checkboxes instead of reusing
        // the existing checkboxes here because new checkboxes don't alter the
        // submitted form. If we'd automatically check existing checkboxes, the
        // permission table would be polluted with redundant entries. This
        // is deliberate, but desirable when we automatically check them.
        var $dummy = $('<input type="checkbox" class="dummy-checkbox" disabled="disabled" checked="checked" />').hide();
        $('input[type=checkbox]', this).not('.rid-2, .rid-1').addClass('real-checkbox').each(function() {
          $dummy.clone().insertAfter(this);
        });
        // We add the dependency behavior.
        add_dependency_recursive(Drupal.settings.cascading_roles.tree, this);

        // Re-insert the table into the DOM.
        $ancestor[method]($table);
      }).map(processTable);

      function add_dependency_recursive(tree, table) {
        $.each(tree, function(parentRid, children) {
          // We had a class to each dependent checkbox (real and dummy one).
          if(children != undefined) {
            var className = 'rid-' + parentRid + '-dependent';
            var filters = [];
            $.each(children, function(childRid) {
              filters.push('.rid-' + childRid);
            });
            // The title of the dummy checkbox.
            var title = Drupal.t("This permission is inherited from the !name user role.", {
              '!name' : Drupal.settings.cascading_roles.names[parentRid]
            });
            $('input.real-checkbox', table).filter(filters.join(',')).addClass(className).next('input.dummy-checkbox').addClass(className).attr('title', title);

            // We add dynamic checking on click.
            $('input[type=checkbox].rid-' + parentRid, table).data('rid', parentRid).bind('click.permissions', self.toggle);

            // We do it again deeper in the tree.
            add_dependency_recursive(children, table);
          }
        });
      }

      function processTable() {
        // We only need to toggle the real checked checkboxes, and it will update
        // the table.
        // .triggerHandler() cannot be used here, as it only affects the first
        // element.
        $('input[type=checkbox]', this).filter(':checked').filter('.real-checkbox, .rid-2').each(Drupal.behaviors.permissions.toggle);
      }

    },
    /**
     * Toggles all dummy checkboxes based on the checkboxes' state.
     *
     * If the parent checkbox is checked, the checked and disabled
     * checkboxes are shown, the real checkboxes otherwise.
     */
    toggle : function() {
      var parentCheckbox = this, $row = $(this).closest('tr');
      var rid = $(parentCheckbox).data('rid');
      // Handle programmatically 'checked' chexboxes (the hidden and real one
      // can be unchecked but we must not mind).
      var checked = $(parentCheckbox).is(':visible') ? parentCheckbox.checked : true;
      if(checked) {
        // We programatically 'checked' this checkbox, so we need to 'check'
        // the dependent ones if the real one wasn't already checked : we cascade.
        $row.find('.real-checkbox.rid-' + rid + '-dependent').filter(':visible').each(myHide).map(cascade);
        $row.find('.dummy-checkbox.rid-' + rid + '-dependent').not(':visible').each(myShow);
      }
      else {
        // We restore the real checkbox, which may have dependent ones and
        // not be checked, so we cascade.
        $row.find('.real-checkbox.rid-' + rid + '-dependent').not(':visible').each(myShow).map(cascade);
        $row.find('.dummy-checkbox.rid-' + rid + '-dependent').filter(':visible').each(myHide);
      }
      // jQuery performs too many layout calculations for .hide() and
      // .show(), leading to a major page rendering lag on sites with many
      // roles and permissions. Therefore, we toggle visibility directly.
      function myShow() {
        this.style.display = '';
      }

      function myHide() {
        this.style.display = 'none';
      }

      function cascade() {
        if(!$(this).checked && $(this).data('rid')) {
          $(this).map(Drupal.behaviors.permissions.toggle);
        }
      }

    }
  };

})(jQuery);
